pdf:
	asciidoctor-pdf -a pdf-style=./resources/theme.yml -a pdf-fontsdir=./resources/fonts asciidoc/index.adoc -D build/pdf

clean:
	rm -rf build